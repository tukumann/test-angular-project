import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IGroup } from "../models/IGroup";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {


  data: IGroup[] = [];

  myForm: FormGroup = new FormGroup({
    "userName": new FormControl("Tom", Validators.required),
    "userSurname": new FormControl("", Validators.required),
    "userEmail": new FormControl("", [
                Validators.required,
                Validators.email
    ]),
    "personalPhone": new FormControl("", Validators.pattern("[0-9]{11}")),
    "mobilePhone": new FormControl("", Validators.pattern("[0-9]{9}"))
});

  constructor() { }

  ngOnInit(): void {
  }

  submit(){

    let resource = JSON.stringify(this.myForm.value);
    let parsedResource = JSON.parse(resource)
    console.log(resource);
    this.data.push(parsedResource);
    console.log(this.data);

}

}
