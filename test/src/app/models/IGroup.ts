export interface IGroup {
  id?: number;
  userName: string;
  userSurame: string;
  userEmail: string,
  personalPhone: string,
  mobilePhone: string
}
